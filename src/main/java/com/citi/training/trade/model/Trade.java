package com.citi.training.trade.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Trade {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(length=10)
	private String stock;
	private int buy;
	private double price;
	private int volume;

	public Trade() {}

	public Trade(long id, String stock, int buy, double price, int volume) {
		super();
		this.id = id;
		this.stock = stock;
		this.buy = buy;
		this.price = price;
		this.volume = volume;
	}

	
	public Trade(long id, String stock, int buy) {
		super();
		this.id = id;
		this.stock = stock;
		this.buy = buy;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public int getBuy() {
		return buy;
	}

	public void setBuy(int buy) {
		this.buy = buy;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	@Override
	public String toString() {
		return "Trade [id=" + id + ", stock=" + stock + ", buy=" + buy + ", price=" + price + ", volume=" + volume
				+ "]";
	}

	public boolean equals(Trade trade) {
		return this.id == trade.getId() && this.stock.equals(trade.getStock()) && this.buy == trade.getBuy()
				&& this.price == trade.getPrice() && this.volume == trade.getVolume();
	}

}
