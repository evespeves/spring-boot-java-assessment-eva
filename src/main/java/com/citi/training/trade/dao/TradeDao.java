package com.citi.training.trade.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trade.model.Trade;

public interface TradeDao extends CrudRepository<Trade, Long> {
}
