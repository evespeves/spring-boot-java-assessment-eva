package com.citi.training.trade.rest;

import org.slf4j.Logger;


import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import com.citi.training.trade.dao.TradeDao;
import com.citi.training.trade.model.Trade;
import com.citi.training.trade.rest.TradeController;


/**
 * 
 * This is the rest interface to see trade
 * @author Eva
 * 
 * <p> This is a class that adheres to RESTful paths and response codes</p>
 * 
 * <a href="localhost:8081/swagger-ui.html">Link to swagger ui</a>
 * 
 *
 * @see Trade
 */

@RestController
@RequestMapping("/trades")
public class TradeController {
	
	private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

    @Autowired
    private TradeDao tradeDao;

    @RequestMapping(method=RequestMethod.GET)
    public Iterable<Trade> findAll() {
        LOG.info("HTTP GET findAll() trades");
        return tradeDao.findAll();
    }
    
    /**
     * Find a particular trade records by its numerical id
     * @param id the id of the record to find
     * @return The trade object that was found.
     */

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Trade findById(@PathVariable long id) {
        LOG.info("HTTP GET findById() when id=[" + id + "]");
        return tradeDao.findById(id).get();
    }

    /**
     * Save particular trade 
     * @return The trade object that was saved.
     */
    @RequestMapping(method=RequestMethod.POST)
    public HttpEntity<Trade> save(@RequestBody Trade trade) {
        LOG.info("HTTP POST save() the trade=[" + trade + "]");
        return new ResponseEntity<Trade> (tradeDao.save(trade), HttpStatus.CREATED);
    }

    
    /**
     * delete a particular trade records by its numerical id
     * @param id the id of the record to find
     */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value=HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long id) {
        LOG.info("HTTP DELETE delete() the trade with the id=[" + id + "]");
        tradeDao.deleteById(id);
    }

}
