package com.citi.training.trade.rest;

import java.util.NoSuchElementException;

import javax.annotation.Priority;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Priority(1)
public class DefaultExceptionHandler {
   
    private static final Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);
       
    @ExceptionHandler(value = {EntityNotFoundException.class})
    public ResponseEntity<Object> tradeNotFoundExceptionHandler(
            HttpServletRequest request, EntityNotFoundException ex) {
        logger.warn(ex.toString());
        return new ResponseEntity<>("{\"message\": \"" + ex.getMessage() + "\"}",
                                    HttpStatus.NOT_FOUND);
    }
   
    @ExceptionHandler(value = {EmptyResultDataAccessException.class})
    public ResponseEntity<Object> tradeDeleteWhenNotThereException(
                                            HttpServletRequest request,
                                            EmptyResultDataAccessException ex) {
        logger.warn(ex.toString());
        return new ResponseEntity<>("{\"message\": \"" + ex.getMessage() + "\"}",
                                    HttpStatus.NOT_FOUND);
    }
   
}
