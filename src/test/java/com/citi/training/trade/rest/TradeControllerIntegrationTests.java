package com.citi.training.trade.rest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.citi.training.trade.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class TradeControllerIntegrationTests {

	private static final Logger LOG = LoggerFactory.getLogger(TradeControllerIntegrationTests.class);

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void getTrade_returnsTrade() {
		long testId = 878;
		String testStock = "LON";
		int testBuy = 0;
		double testPrice = 284.99;
		int testVolume = 200;

		ResponseEntity<Trade> createTradeResponse = restTemplate.postForEntity("/trades",
				new Trade(testId, testStock, testBuy, testPrice, testVolume), Trade.class);

		LOG.info("Create Trade response: " + createTradeResponse.getBody());
		assertEquals(HttpStatus.CREATED, createTradeResponse.getStatusCode());
		assertEquals("created Trade stock should equal testStock", testStock, createTradeResponse.getBody().getStock());
		assertEquals("created Trade buy should equal testBuy", testBuy, createTradeResponse.getBody().getBuy());
		assertEquals("created Trade price should equal testPrice", testPrice, createTradeResponse.getBody().getPrice(),
				0.0001);
		assertEquals("created Trade volume should equal testVolume", testVolume,
				createTradeResponse.getBody().getVolume());

		ResponseEntity<Trade> findTradeResponse = restTemplate
				.getForEntity("/trades/" + createTradeResponse.getBody().getId(), Trade.class);

		LOG.info("FindById response: " + findTradeResponse.getBody());
		System.out.println(findTradeResponse.getStatusCode());
		assertEquals(HttpStatus.OK, findTradeResponse.getStatusCode());
		assertEquals("created Trade stock should equal testStock", testStock, createTradeResponse.getBody().getStock());
		assertEquals("created Trade buy should equal testBuy", testBuy, createTradeResponse.getBody().getBuy());
		assertEquals("created Trade price should equal testPrice", testPrice, createTradeResponse.getBody().getPrice(),
				0.0001);
		assertEquals("created Trade volume should equal testVolume", testVolume,
				createTradeResponse.getBody().getVolume());
		
		
	}

}
