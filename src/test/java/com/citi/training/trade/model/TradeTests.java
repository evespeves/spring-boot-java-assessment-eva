package com.citi.training.trade.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


import com.citi.training.trade.model.Trade;

public class TradeTests {

	private long testId = 21;;
	private String testStock = "APPL";
	private int testBuy = 1;
	private double testPrice = 24.5431;
	private int testVolume = 100;

	@Test
	public void test_trade_defaultConstructorAndSetters() {
		Trade testTrade = new Trade();

		testTrade.setId(testId);
		testTrade.setStock(testStock);
		testTrade.setBuy(testBuy);
		testTrade.setPrice(testPrice);
		testTrade.setVolume(testVolume);

		assertEquals("Trade id should be equal to testId", testId, testTrade.getId());
		assertEquals("Trade stock should be equal to testStock", testStock, testTrade.getStock());
		assertEquals("Trade buy should be equal to testBuy", testBuy, testTrade.getBuy());
		assertEquals("Trade price should be equal to testPrice", testPrice, testTrade.getPrice(), 0.0001);
		assertEquals("Trade volume name should be equal to testVolume", testVolume, testTrade.getVolume());
	}

	@Test
	public void test_trade_fullConstructor() {
		Trade testTrade = new Trade(testId, testStock, testBuy);
		assertEquals("Trade id should be equal to testId", testId, testTrade.getId());
		assertEquals("Trade stock should be equal to testStock", testStock, testTrade.getStock());
		assertEquals("Trade buy should be equal to testBuy", testBuy, testTrade.getBuy());
	}
	
	@Test
    public void test_trade_toString() {
		Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);

        assertTrue("toString should contain testId",
                   testTrade.toString().contains(Long.toString(testId)));
        assertTrue("toString should contain testStock",
                testTrade.toString().contains(testStock));
        assertTrue("toString should contain testCompanyName",
                testTrade.toString().contains(Integer.toString(testBuy)));
        assertTrue("toString should contain testPrice",
                testTrade.toString().contains(Double.toString(testPrice)));
        assertTrue("toString should contain testCompanyName",
                testTrade.toString().contains(Integer.toString(testVolume)));
    }

	
	@Test
    public void test_trade_equals() {
		Trade firstTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);

		Trade compareTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);
        assertFalse("stocks for test should not be the same object",
        		firstTrade == compareTrade);
        assertTrue("stocks for test should be found to be equal",
        		firstTrade.equals(compareTrade));
    }

}
