DROP DATABASE IF EXISTS tradedb;

CREATE DATABASE tradedb;
use tradedb;


CREATE TABLE `trade` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `stock` varchar(10) DEFAULT NULL,
  `buy` int NOT NULL,
  `price` double NOT NULL,
  `volume` int NOT NULL, 
  PRIMARY KEY (`id`)
);

CREATE TABLE `company` (
  `companyId` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `tradeId` int NOT NULL,
  PRIMARY KEY (`companyId`)
);